package com.example.demo.service.impl;

import com.example.demo.pojo.quartz.Job1;
import com.example.demo.pojo.quartz.Quartz;
import com.example.demo.service.QuartzService;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author T480
 * 定时任务执行业务类
 */
@Service
public class QuartzServiceImpl implements QuartzService {


    private static final SchedulerFactory schedulerFactory = new StdSchedulerFactory();

    private String job1="日巡";

    private String job2="夜巡";


    /**
     * @param oneMap 定时任务对象
     * @Description: 添加一个定时任务
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public void addJob(Map<String, Object> oneMap) {
        try {
            Scheduler sched = schedulerFactory.getScheduler();
            // 根据任务名，任务组，任务执行类判断执行不同的任务逻辑
            String jobName = oneMap.get("jhmc").toString();
            String jobGroupName = oneMap.get("xjlx").toString();
            String cron = oneMap.get("cron").toString();
            JobDetail jobDetail = null;
            if (job1.equals(oneMap.get("xjlx").toString())) {
                jobDetail = JobBuilder.newJob(Job1.class).withIdentity(jobName, jobGroupName).build();
            } else if (job2.equals(oneMap.get("xjlx").toString())) {
                jobDetail = JobBuilder.newJob(Job1.class).withIdentity(jobName, jobGroupName).build();
            }

            // 触发器
            TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
            // 触发器名,触发器组
            triggerBuilder.withIdentity(jobName, jobGroupName);
            triggerBuilder.startNow();
            // 触发器时间设定
            triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(cron));
            // 创建Trigger对象
            CronTrigger trigger = (CronTrigger) triggerBuilder.build();
            // 调度容器设置JobDetail和Trigger
            sched.scheduleJob(jobDetail, trigger);
            // 启动
            if (!sched.isShutdown()) {
                sched.start();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 关闭传入的定时任务
     *
     * @param entity
     * @return
     */
    public String closeTasking(Quartz entity) {

        String mes = null;
        try {
            /*关闭定时开启状态*/
            //先去数据库关闭将定时计划的状态变更为关闭 xxxx

            //在关闭定时任务的触发器
            /*关闭执行中的触发器 */
            Scheduler sched = schedulerFactory.getScheduler();

            String triggerName = entity.getJhmc();
            String triggerGroupName = entity.getXjlx().toString();

            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
            // 停止触发器
            sched.pauseTrigger(triggerKey);
            // 移除触发器
            sched.unscheduleJob(triggerKey);
            // 删除任务
            sched.deleteJob(JobKey.jobKey(triggerName, triggerGroupName));
        } catch (Exception e) {

            return "关闭失败";
        }
        return "关闭成功";
    }

    /**
     * 在系统启动时添加需要运行的定时任务
     */
    public void contextInitialized() {
        //系统启动时查询需要循环的定时任务--dataDictionary
        List<Map<String, Object>> dataDictionary = new ArrayList<>();
        //添加定时任务触发器-dataDictionary
        for (Map<String, Object> oneMap : dataDictionary) {
            try {
                //添加定时任务
                this.addJob(oneMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
