package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.common.Response;
import com.example.demo.mapper.UserMapper;
import com.example.demo.pojo.RequestUserPoJo;
import com.example.demo.pojo.UserPoJo;
import com.example.demo.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;


/**
 * @author T480
 */
@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserMapper userMapper;

    @Override
    public Response inserOrUpdateUser(RequestUserPoJo requestUserPoJo) {

        UserPoJo user = new UserPoJo();
        BeanUtils.copyProperties(requestUserPoJo,user);
        try{
            if (StringUtils.isEmpty(user.getId())){
                String uuid= UUID.randomUUID().toString().replace("-", "");
                user.setId(uuid);
                user.setPhone("0/3 * * * * ?");
                userMapper.insert(user);
            }else {

                userMapper.updateById(user);
            }
        }catch (Exception e){
            return  Response.error("500",e);

        }
        return Response.fail("200","操作成功!");

    }

    @Override
    public Response deleteByUserId(List<String> list ) {

        try {
                if (list!=null && list.size()>0){
                    userMapper.deleteListUserId(list);
                }

           return Response.error("200","删除成功");
        }catch (Exception e){
            return   Response.error("500",e);
        }

    }

    @Override
    public Response selectPageUser(RequestUserPoJo requestUserPoJo) {

        try {


        Page page =new Page<>();
        page.setPages(requestUserPoJo.getPage());
        page.setSize(requestUserPoJo.getSize());

        QueryWrapper<UserPoJo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name",requestUserPoJo.getName());
        Page page1 = userMapper.selectPage(page,queryWrapper);
//        查询总数
        Integer count = userMapper.selectCount(queryWrapper);
        page1.setTotal(count);
        Response.ok(page1);
        return Response.ok(page1);

        }catch (Exception e){
            return    Response.error("500",e);
        }

    }

}
