package com.example.demo.service;

import com.example.demo.common.Response;
import com.example.demo.pojo.RequestUserPoJo;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author T480
 */

public interface UserService {

    /**
     * 保存用户人员内信息
     * @param requestUserPoJo
     *
     * @return
     */
    Response inserOrUpdateUser(@Param("RequestUserPoJo")RequestUserPoJo requestUserPoJo);

    Response deleteByUserId(List<String> list);


    Response selectPageUser(@Param("RequestUserPoJo")RequestUserPoJo requestUserPoJo);
}
