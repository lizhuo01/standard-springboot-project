package com.example.demo.service;

import lombok.extern.slf4j.Slf4j;
import net.qiyuesuo.sdk.SDKClient;
import net.qiyuesuo.sdk.api.ContractService;
import net.qiyuesuo.sdk.api.SignService;
import net.qiyuesuo.sdk.bean.company.CompanySignRequest;
import net.qiyuesuo.sdk.bean.company.TenantType;
import net.qiyuesuo.sdk.bean.contract.*;
import net.qiyuesuo.sdk.bean.document.DownloadDocRequest;
import net.qiyuesuo.sdk.bean.sign.Signatory;
import net.qiyuesuo.sdk.common.exception.PrivateAppException;
import net.qiyuesuo.sdk.common.utils.TimeUtils;
import net.qiyuesuo.sdk.impl.ContractServiceImpl;
import net.qiyuesuo.sdk.impl.SignServiceImpl;
import net.qiyuesuo.v2sdk.SdkV2Client;
import net.qiyuesuo.v2sdk.bean.CompanyRequest;
import net.qiyuesuo.v2sdk.bean.UserRequest;
import net.qiyuesuo.v2sdk.request.SilentCompanySignAuthRequest;
import net.qiyuesuo.v2sdk.response.SdkResponse;
import net.qiyuesuo.v2sdk.response.SilentSignAuthPageResponse;
import net.qiyuesuo.v2sdk.utils.JSONUtils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 电子签章
 *
 * @author dashoo
 * @email dashoo@163.com
 * @date 2022-02-19 19:12:30
 */
@Slf4j
@Service
public class ElectronicSignatureService {

//    @Value("${qiyuesuo.url}")
    private String url ="http://127.0.0.1:8081";
//    @Value("${qiyuesuo.appToken}")
    private String appToken= "gaKiG5MpLD";
//    @Value("${qiyuesuo.appSecret}")
    private String appSecret= "5b9jodsACCx4iYSIWTjvHHXBupCCK4";


    // 调用
    public void getSignatureUrl(Map<String, String> params, HttpServletResponse response) throws Exception {
        Long documentId = this.createContractDocument(params);
        Long contractId = this.createContract(documentId);
        this.signature(contractId, documentId, response);
    }

    // 创建合同文档
    private Long createContractDocument(Map<String, String> params) throws Exception {
        // 1.1.1.2用模板创建合同文档->1.1.4.1创建合同->1.2.2.1公司公章签署->1.4.2.2下载合同文档
        // 用模板创建合同文档

        // 设置参数：图片参数
//        FileInputStream in = new FileInputStream("/Users/test/develop/file/eployee3.png");
//        byte[] bs = IOUtils.readStreamAsByteArray(in);
//        IOUtils.safeClose(in);
//        String imgBase64 = Base64Utils.encode(bs); // 图片的base64格式
//        Map<String,String> imgParam = new HashMap<>(); // 图片参数为json格式，包含value和fileName两个参数
//        imgParam.put("value","data:image/png;base64," + imgBase64);
//        imgParam.put("fileName", "测试图片");
//        String imgParamStr = JSONUtils.toJson(imgParam);
//        params.put("图片参数", imgParamStr);
        // 添加水印
        List<WaterMarkContent> list = new ArrayList<WaterMarkContent>();
//        WaterMarkContent waterMarkContent1 = new WaterMarkContent();
//        waterMarkContent1.setLocation(WaterMarkLocation.UPPER_LEFT);
//        waterMarkContent1.setImageBase64("/9j/4AAQSkZJR..."); // 图片的base64格式
//        WaterMarkContent waterMarkContent2 = new WaterMarkContent("文字水印2", 15, WaterMarkLocation.UPPER_RIGHT);
//        list.addAll(Arrays.asList(waterMarkContent1,waterMarkContent2));
        ContractService contractService = new ContractServiceImpl(new SDKClient(this.url, this.appToken, this.appSecret));
        log.info("地址是", this.url);
        Long documentId = contractService.createDocument(2961531550726545618l, params, "html模板文档", list);
        log.info("创建合同文件成功,documentId:{}", documentId); // 2960131585806983180

        return documentId;

    }

    // 创建合同
    private Long createContract(Long documentId) throws Exception {

        //方法调用
        CreateContractRequest createContractRequest = new CreateContractRequest();
        createContractRequest.setCategoryId(2960081670917251168L);
        createContractRequest.setEndTime(TimeUtils.format(TimeUtils.after(new Date(), 1), TimeUtils.STANDARD_PATTERN));
        createContractRequest.setSend(true);
        List<Long> ids = new ArrayList<>();
        ids.add(documentId);
        createContractRequest.setDocuments(ids);
        createContractRequest.setMustSign(true);
        createContractRequest.setExtraSign(false);
        createContractRequest.setAutoCreateCounterSign(true);

        createContractRequest.setSubject("测试合同05-05");
        createContractRequest.setCreatorName("张三");
        createContractRequest.setCreatorContact("19000000000");
        // createContractRequest.setSend(false);

        List<Signatory> signatories = new ArrayList<Signatory>();
        // 发起方
        Signatory signatory1 = new Signatory();
        signatory1.setContact("19000000000");
        signatory1.setTenantType(TenantType.CORPORATE);
        signatory1.setTenantName("天津大港油田");

        // 添加签署方
        signatories.add(signatory1);
        createContractRequest.setSignatories(signatories);

        // 设置模板参数
        DocumentParam param1 = new DocumentParam("param1", "val1");
        DocumentParam param2 = new DocumentParam("param2", "val2");
        createContractRequest.addDocumentParam(param1);
        createContractRequest.addDocumentParam(param2);

        ContractService contractService = new ContractServiceImpl(new SDKClient(this.url, this.appToken, this.appSecret));
        Long contractId = contractService.createContractByCategory(createContractRequest);
        log.info("创建合同成功,contractId:" + contractId);// 2960131593071517729
        return contractId;

    }

    // 静默签署授权链接
    private String authorization() throws Exception {

        // 1.2.1.2 企业印章静默签署授权链接-新版需要授权
        SilentCompanySignAuthRequest request = new SilentCompanySignAuthRequest();
        CompanyRequest applyCompany = new CompanyRequest();
        applyCompany.setName("天津大港油田");
        request.setApplyCompany(applyCompany);
        CompanyRequest authCompany = new CompanyRequest();
        authCompany.setName("天津大港油田");
        request.setAuthCompany(authCompany);
        UserRequest userRequest = new UserRequest();
        userRequest.setMobile("10012345678");
        request.setAuthUser(userRequest);
        request.setAuthEndDate("2022-12-29");
        request.setAuthScope("测试范围");
        request.setCallbackUrl("https://www.baidu.com");
        SdkV2Client sdkClient =  new SdkV2Client(this.url, this.appToken, this.appSecret);
        String response = sdkClient.service(request);
        SdkResponse<SilentSignAuthPageResponse> responseObj = JSONUtils.toQysResponse(response, SilentSignAuthPageResponse.class);
        String url = "";
        if(responseObj.getCode() == 0) {
            SilentSignAuthPageResponse result = responseObj.getResult();
            log.info("授权页面链接：{}", result.getUrl());
            url = result.getUrl();
        } else {
            log.info("请求失败，错误码:{}，错误信息:{}", responseObj.getCode(), responseObj.getMessage());
        }
        return url;
    }

    // 静默签署 和 下载
    private void signature(Long contractId, Long documentId, HttpServletResponse response) throws Exception {

        // 老版本的静默签章
        CompanySignRequest request = new CompanySignRequest();
        request.setContractId(contractId);
        request.setTenantName("天津大港油田");
        Stamper stamper = new Stamper();
        stamper.setKeywordIndex(2);
        stamper.setSealId(2960093309683032201L);
        stamper.setType(StamperType.SEAL_CORPORATE);
        stamper.setDocumentId(documentId);
        request.addStamper(stamper);
        SignService signService = new SignServiceImpl(new SDKClient(this.url, this.appToken, this.appSecret));
        signService.signByCompany(request);
        log.info("公司公章签署完成");

        // 静默签章-公司公章签署 新版本的需要授权
//        ContractSignCompanyRequest request1 = new ContractSignCompanyRequest();
//        ContractRequest contractRequest = new ContractRequest(contractId, null);
//        request1.setContract(contractRequest);
//        CompanyRequest companyRequest = new CompanyRequest();
//        companyRequest.setName("天津大港油田");
//        request1.setCompany(companyRequest);
//        request1.setSealId(2960093309683032201L);
//        StamperBean stamperBean = new StamperBean();
//        stamperBean.setDocumentId(documentId);
//        stamperBean.setType("SEAL_CORPORATE");
//        stamperBean.setSealId(2960093309683032201L);
//        stamperBean.setPage(1);
//        stamperBean.setOffsetX(0.5D);
//        stamperBean.setOffsetY(0.5D);
//        request1.addStamper(stamperBean);
//        String response1 = sdkClient.service(request1);
//        SdkResponse responseObj1 = JSONUtils.toQysResponse(response1);
//        if(responseObj1.getCode() == 0) {
//            log.info("请求成功");
//        } else {
//            log.info("请求失败，错误码:{}，错误信息:{}", responseObj1.getCode(), responseObj1.getMessage());
//        }
        this.download(documentId, response);

    }

    public void download(Long documentId, HttpServletResponse response) throws Exception {

        // 下载合同文档
        OutputStream outputStream = response.getOutputStream();
        DownloadDocRequest request2 = new DownloadDocRequest();
//        FileOutputStream fos = new FileOutputStream("http://10.76.4.250/fssbuk/未命名1.pdf");
//        FileOutputStream fos = new FileOutputStream("/tmp/test.pdf");
        request2.setDocumentId(documentId);
//        request2.setContact("10000000001");
//        request2.setName("演示用户");
        request2.setOutputStream(outputStream);
        ContractService contractService = new ContractServiceImpl(new SDKClient(this.url, this.appToken, this.appSecret));
        contractService.downloadDoc(request2);
//        outputStream.flush();
//        outputStream.close();
        log.info("下载文件成功");

    }

//    @Test
//    public void test () throws PrivateAppException, FileNotFoundException {
//
//        DownloadDocRequest request2 = new DownloadDocRequest();
//        FileOutputStream fos = new FileOutputStream("/Users/mac/dagang/aozheyunshu/test.pdf");
//        request2.setDocumentId(2967273625094090785L);
//        request2.setOutputStream(fos);
//        ContractService contractService = new ContractServiceImpl(new SDKClient("https://privopen.qiyuesuo.me", "IUIjEn0Wu2", "zo8dq7vKqqzoiDVPRIJNWhQSH1D9a2"));
//        contractService.downloadDoc(request2);
//        log.info("下载文件成功");
//
//    }
}

