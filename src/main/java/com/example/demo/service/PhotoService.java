package com.example.demo.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author T480
 */

public interface PhotoService {


    Boolean add(MultipartFile file);
}
