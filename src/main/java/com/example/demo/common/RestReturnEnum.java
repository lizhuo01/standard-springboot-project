package com.example.demo.common;

public enum RestReturnEnum {
    SUCCESS(10000, "操作成功"),
    RESULT_NULL(201, "请求结果为空"),
    FAIL(-1, "操作失败"),
    BAD_REQUEST(400, "错误请求"),
    FORBIDDEN(403, "无权限访问"),
    UNAUTHORIZED(401, "未认证"),
    NOT_FOUND(404, "未找到"),
    SYSTEM_UNAUTHORIZED(409, "系统未授权或者已过期"),
    EXCEPTION(500, "系统异常"),
    ACCOUNT_LOCKED(455, "当前账号已禁用，请向管理员申请访问权限！"),
    VALID_ERROR(400, "校验错误"),
    SYSTEM_NOT_INIT(410, "系统未初始化"),
    SYS_ROLE_OPERATE(2029, "系统角色不能操作"),


    /* 参数错误：10001-19999 */
    PARAM_IS_INVALID(1001, "参数无效"),
    PARAM_IS_BLANK(1002, "参数{0}为空"),
    PARAM_TYPE_BIND_ERROR(1003, "参数格式错误"),
    PARAM_NOT_COMPLETE(1004, "参数缺失"),
    PARAM_URL_LAYOUT_ERROR(1005, "参数url格式错误"),
    PARAM_IS_REPEAT(1006, "参数重复"),
    PARAM_RESULT_NULL(1007, "参数{0}查询结果为空，参数无效"),
    PARAM_TRUE_IS_INVALID(1008, "参数{0}无效或为空"),
    /* 组件错误：20001-29999*/
    MODULE_HAS_EXISTED(20001, "组件已存在"),
    MODULE_IN_USED(20002, "组件在使用中[{0}]"),
    MODULE_PACKAGE_CONFIG_ILLEGAL(20003, "组件包配置文件不正确"),
    MODULE_NOT_EXISTED(20004, "组件不存在，请先导入以下组件[{0}]"),
    MODULE_PROPERTY_NOT_EXISTED(20005, "组件配置不存在"),
    MODULE_ZIP_UPLOAD_FAIL(20006, "组件包上传失败"),
    RESOURCE_HAS_EXISTED(20007, "素材已存在"),
    RESOURCE_HAS_SAME_NAME_MODULE(20008, "已存在同名的组件"),
    SYSTEM_RESOURCE_TYPE_CONFIG(20009, "系统资源类型配置为空"),
    MODULE_BIND_WEBSOCKET(20010, "组件已绑定当前事件服务"),
    SENDER_EVENT_ONLY_ONE(20011,"websocket服务事件只能有一个组件作为发送方"),

    /*操作业务错误*/
    OPERATE_ERROR(3001, "操作失败"),
    GET_OBJECT(3002, "{0}"),
    APP_NOT_EXIST(3002, "应用不存在"),
    FILE_NOT_EXIST(3003, "文件不存在"),
    FILE_NOT_DO(3004, "路径为文件夹,不可操作"),
    FILE_DELETE_ROOT_ORG(3005, "根组织不可删除"),
    FILE_LOAD_FAIL(3006, "文件上传失败"),
    FILE_LOAD_EMPTY(3007, "文件为空"),
    FILE_DOWNLOAD_FAIL(3008, "下载失败"),
    FILE_LAYOUT_ERROR(3009, "文件格式错误"),
    EXIST_SAME_GROUP_NAME(3010, "分组名已存在"),
    EXIST_SAME_APP_NAME(3011,"应用名已存在"),
    GROUP_NOT_EXIST(3009, "分组不存在"),
    GATEWAY_ROUTE_REMOVE_ERROR(3012, "[{0}]路由删除失败"),
    LOGOUT_ERROR(3010, "退出登录失败,请检查系统"),
    FILE_OPEN_FAIL(3011, "文件打开失败"),
    FEIGN_SERVICE_ERROR(3111, "远程服务不可用"),

    ENCRYPT_ALGORITHM_NOT_EXIST(3011, "[{0}]加密算法不存在"),
    ENCRYPT_PRI_PUB_SAME(3012, "[{0}]加密算法公私钥不能相同"),
    ENCRYPT_KEY_LENGTH_ERROR(3013, "[{0}]加密算法密钥长度错误,必须为[{1}]位"),
    ENCRYPT_NEED_USER_MARK(3014, "[{0}]加密算法需要定义一个用户标识"),
    RESOURCE_TYPE_NOE_EXIST(3015, "资源类型不存在"),
    ENCRYPT_PARAM_EXISTS(3016, "[{0}]加密算法缺少[{1}]"),
    WATER_MARK_ERROR(3017, "[{0}]水印失败"),
    WATER_MARK_NOT_EXISTS(2018, "[{0}]水印策略不存在"),

    API_CONFIG_FAILURE(3019, "api 安全配置失败"),
    FILE_STREAM_ERROR(3020, "文件流异常"),
    ENCRYPT_ERROR(3021, "解密失败"),
    DATA_SOURCE_TYPE_NOT_EXISTS(3022, "无法适配[{0}]数据源"),
    RESOURCE_GROUP_EXISTS(3023, "资源分组[{0}]已存在"),
    RESOURCE_GROUP_LEVEL_ERROR(3024, "资源分组层级已达上限,无法在此分组下创建新分组"),
    RESOURCE_GROUP_NOT_DELETE(3025, "此资源分组下存在资源,请先将资源移除再删除"),
    RESOURCE_GROUP_NOT_EXISTS(3026, "资源分组不存在"),
    WATERMARK_NAME_EXIST(3027, "水印名称重复"),
    RESOURCE_NAME_EXIST(3028, "资源名称重复"),
    EMAIL_SEND_ERROR(3029, "邮箱发送失败,请重试或联系管理员"),
    EMAIL_EXISTS(3030, "邮箱已存在"),
    ROLE_NAME_EXISTS(3031, "角色名称已存在"),
    ROLE_NAME_NO_EXISTS(10000, "角色名称不存在"),
    CONTAINMENT_NAME_EXIST(3032, "防泄漏策略名称重复"),
    CONTAINMENT_RULE_NAME_EXIST(3033, "防泄漏规则名称重复"),
    RESOURCE_NOT_EXISTS(3034, "资源不存在"),
    RESOURCE_GROUP_NOT_DO(3035, "此资源分组不能操作"),
    RESOURCE_SHARING_NOT_DO(3036, "此资源处于共享中,无法操作"),
    RESOURCE_API_VISIT_IMPACT(3137, "[{0}] 资源触发防泄漏规则,无法被访问"),
    RESOURCE_FILE_CREATE_IMPACT(3137, "[{0}] 文件资源触发防泄漏规则,无法被创建"),
    MESSAGE_NOT_EXISTS(3037, "消息不存在"),
    CHECK_NOT_NULL(3037, "已审核"),
    NOT_UPDATE_RESOURCE_TYPE(3038, "资源类型无法修改"),
    DATASOURCE_NAME_EXISTS(3039, "数据源名称已存在"),
    DATASOURCE_NOT_EXISTS(3040, "数据源不存在"),
    DATASOURCE_NOT_DELETE(3041, "数据源拥有关联资源无法删除,请先处理资源"),
    NOT_COMMENT_ERROR(3042, "您没有使用过此资源,无法评价"),
    RESOURCE_UNAUTHORIZED(3043, "无权限访问此资源"),
    RESOURCE_NOT_SHARING(3044, "资源未开启共享状态,不可访问"),
    ROLE_NOT_EXISTS(3045, "角色不存在"),
    ROLE_NOT_DO(3045, "系统默认角色,无法操作"),
    ROLE_EXISTS_USER(3046, "该角色下存在用户,无法删除"),
    RESOURCE_API_NOT_EXISTS(3048, "资源抽象api不存在"),
    RESOURCE_STATUS_NOT_DO(3049, "此资源状态无法操作"),
    RESOURCE_NOT_EXISTS_OR_UNAUTHORIZED(3050, "资源不存在或无权限访问"),
    RESOURCE_EXPIRED_OR_UNAUTHORIZED(3051, "资源已过期或无权限访问"),
    RESOURCE_CHECKING_NOT_DO(3052, "此资源处于审核中,无法操作"),
    COMMENT_NOT_EXISTS(3053, "评论不存在"),
    COMMENT_NOT_DELETE_UNAUTHORIZED(3054, "无权限删除此评论"),
    RESOURCE_REPLY_UNAUTHORIZED(3055, "无权限审核此资源"),
    RESOURCE_NOT_REPLY(3056, "无需审核此资源"),
    DATASOURCE_QUERY_ERROR(3057, "数据源获取失败{0}"),
    DATASOURCE_CONNECTION_ERROR(3058, "测试连接数据源失败"),
    DATASOURCE_OPTION_ERROR(3059, "数据源操作失败"),
    DATASOURCE_L_ERROR(3059, "数据源异常"),
    COMMENT_NOT_POTS_LIKE(3060, "不能重复点赞"),
    COMMENT_CANCEL_POTS_LIKE(3061, "不能重复取消点赞"),
    FILE_FORMAT_ERROR(3062, "文件格式错误,必须为[{0}]文件"),
    FILE_PARSE_ERROR(3063, "文件解析失败,请检查文件内容是否正确"),
    FIRST_EXISTS(3064, "已设置首选"),
    RESOURCE_PARENT_GROUP_NOT_DO(3063, "父资源分组不能操作"),
    FIRST_COUNT(3065, "首选数量不能大于5，请取消其他首选，在设置"),
    MESSAGE_ID_NOT_EXISTS(3066, "消息id为空"),
    RESOURCE_REPLY_NOT_EXISTS(3067, "资源审核单不存在"),
    RESOURCE_REPLY_REPLYED(3068, "资源已审核"),
    RESOURCE_DO_UNAUTHORIZED(3069, "无权限操作此资源"),
    NOTIC_NOT_EXIST(3070, "公告不存在"),
    COMMENT_NOT_RESOURCE_STATUS(3071,"资源未处于共享中,无法评论或回复"),
    DRAFT_NOT_VISIT_RESOURCE(3072,"草稿状态无法进行访问"),
    DATASOURCE_NOT_SUPPORTED(3073, "数据源不支持该操作"),
    MODULE_DATACONFIG_EXCEPTION(3074, "组件数据源配置异常"),
    EXCE_SQL_EXCEPTION(3075, "执行SQL异常"),
    FILTER_EXIST_SAME_NAME(3076, "过滤器重名"),
    APP_SHOW_NEED_PASS(3077, "密码错误，请重新输入"),
    FORBIDDEN_DELETE_GROUP_NAME(3078,"不可删除的分组"),
    PANEL_HAS_DELETED(3079, "面板[{0}]不存在或已删除"),
    CALLBACK_PARAM_EXIST(3080, "组件存在重复的回调参数[{0}]"),
    APP_HSA_REFRECE_APP(3080, "应用引用其他大屏[{0}],请确认是否一起移动"),
    APP_TEMPLATE_EXITED(3081, "应用模板已存在"),
    APP_OR_TEMPLATE_NOT_EXITED(3082, "应用或应用模板不存在"),
    CONTAINER_NOT_DATASOURCE(3083, "容器未配置数据源"),
    MODULE_GET_DATA_ERROR(3084, "组件获取数据异常:{0}"),
    EXIST_SAME_TEMPLATE_NAME(3085, "应用模板名已存在"),
    RESOURCE_NOT_EXIST(3086, "素材不存在"),
    RESOURCE_IN_USED(3087, "素材在使用中[{0}],不允许删除"),
    PARAM_CANNOT_NULL(3087, "回调参数不能为空"),
    RESOURCE_GROUP_NOT_SPACE(3088, "添加素材分组，工作空间id不能为空"),
    GET_APP_SHOW_DATA_NEED_PASS(3089, "大屏非公开状态，获取大屏展示数据，需要正确的密码"),
    EXIST_CRICLE_APP(3090, "循环引用大屏[{0}]"),
    RESOURCE_NOT_UPLOAD(3091, "素材不允许上传，[{0}]组件不存在，请先上传该组件"),
    INTERFACE_REQUEST_EXCEPTION(3092, "接口调用异常:{0}"),
    SQL_IS_NULL(3093, "SQL为空"),
    SQL_EXEC_EXCEPTION(3093, "SQL执行异常:{0}"),


    /*用户权限错误*/
    TOKEN_INVALID(4001, "无效token"),
    TOKEN_EXPIRED(4002, "token已过期"),
    USER_LOGIN_PASSWORD_ERROR(2008, "用户名、密码输入错误"),
    USER_LOGIN_LOCKED(4003, "错误次数太多，账号已被锁定，10分钟后自动解锁"),
    KAPTCHA_INVALID(4004, "验证码已失效"),
    KAPTCHA_ERROR(4005, "验证码错误"),

    /*工作空间 错误：5001~5999*/
    CANNOT_DELETE_DEFAULT_WORK_SPACE(5001, "默认工作空间不可删除"),
    WORK_SPACE_NAME_EXITED(5002, "工作空间名称重复"),
    CANNOT_GREATER_THAN_PROJECT_QUOTA(5003, "不能大于项目最大配额[{0}]"),
    CANNOT_LESS_THAN_PROJECT_QUOTA(5004, "不能小于项目当前已有最小配额[{0}]"),
    REMAIN_QUOTA_IS_0(5005, "当前工作空间剩余配置额度为0"),

    /*工作空间 错误：6001~6999*/
    THEME_NOT_EXIST(6001, "主题不存在");


    private Integer code;

    private String message;

    RestReturnEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}