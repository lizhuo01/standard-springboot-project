package com.example.demo.common;

import lombok.Data;

/**
 * @author T480
 */
@Data
public class Response<T> {

    private String code;


    private String msg;


    private T data;

    public Response(){ }

    public Response(T data, String msg){
        this.code = "000";
        this.data = data;
        this.msg = msg;
    }

    public Response(String code, String msg, T data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 用于前端获取数据或向前端响应数据的时候使用
     */
    public static <T> Response<T> ok(T data){
        return new Response<>("000", "REQUEST SUCCESS", data);
    }

    /**
     * 不需要返回数据，只返回成功的code
     */
    public static Response success(){
        return new Response("000", "REQUEST SUCCESS");}

    /**
     * 不需要返回数据，只返回失败的code
     */
    public static Response fail(String code, String msg)
    {return new Response(code, msg, 0);}

    public static Response error(String code, Object msg){
        return new Response(code, (String)msg, null);
    }

}

