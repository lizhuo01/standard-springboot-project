package com.example.demo.common;

import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author yunshuaiwei
 */
public enum MaterialType {
    /**
     * 判断规则
     **/
    image("image"),
    news("news"),
    video("video"),
    voice("voice");

    MaterialType(String value) {
        this.value = value;
    }

    @Getter
    private final String value;

    public static MaterialType parse(String value) {
        return Stream.of(MaterialType.values())
                .filter(obj -> obj.name().equalsIgnoreCase(value)).findFirst().orElse(null);
    }
}
