package com.example.demo.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author T480
 *
 * 时间工具类
 */
public class DateUtil {

    /**
     * 传入一个时间,传入一个long类型的天数,返回传入时间+天数以后的时间
     */

    public static Date addDate(Date date, long day) throws ParseException {
        // 得到指定日期的毫秒数
        long time = date.getTime();
        // 要加上的天数转换成毫秒数
        day = day*24*60*60*1000;
        // 相加得到新的毫秒数
        time+=day;
        // 将毫秒数转换成日期
        return new Date(time);

    }


    /**
     * Date日期转为Cron格式
     * @param date 时间
     * @param type 日期类型  每天-每月-每年
     * @return
     */
    public String cron(Date date, String type) {

        /**
         * 日期转化为cron表达式
         * @param date
         * @return
         */
        String dateFormat = "ss mm HH dd MM ? yyyy";
        String cron = fmtDateToStr(date, dateFormat);

        String[] array = cron.split(" ");
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            arrayList.add(array[i]);
        }

        /**
         * 下面的代码是为了更细粒度的处理cron格式
         */
        String cronString = "";
        //每天
        if ("每天".equals(type)) {
            arrayList.set(3, "*");
            arrayList.set(4, "*");
            arrayList.remove(6);
            //每月
        } else if ("每月".equals(type)) {
            arrayList.set(4, "*");
            arrayList.remove(6);
            //每年
        } else if ("每年".equals(type)) {
            arrayList.set(6, "*");
        }

        for (String item : arrayList) {
            // 把列表中的每条数据用逗号分割开来，然后拼接成字符串
            cronString += item + " ";
        }

        System.out.println(cron);

        System.out.println(cronString);

        return cronString;
    }

    /**
     * Description:格式化日期,String字符串转化为Date
     *
     * @param date
     * @param dtFormat 例如:yyyy-MM-dd HH:mm:ss yyyyMMdd
     * @return
     */
    public static String fmtDateToStr(Date date, String dtFormat) {
        if (date == null) {
            return "";
        }
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(dtFormat);
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
