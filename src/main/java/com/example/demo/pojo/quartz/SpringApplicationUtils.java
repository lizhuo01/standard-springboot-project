package com.example.demo.pojo.quartz;


import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 该类的可以获取类对象的实例。
 * 由于我们后面写任务逻辑（实现接口 Job类）时可能会需要进行一些方法的注入，例如在任务逻辑中需要使用我们自己写的一些service类，因为Quartz在实例化对象的时候没有经过Spring的处理，那么就意味着在Spring的IOC容器当中没有对应的对象，导致我们自己的一些类无法成功注入。因此需要使用SpringApplicationUtils中的getBean方法手动获取实例对象。
 * @author T480
 */
@Component
public class SpringApplicationUtils implements ApplicationContextAware {
    private static ApplicationContext applicationContext = null;

    /**
     * 获取applicationContext
     *
     * @return
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringApplicationUtils.applicationContext == null) {
            SpringApplicationUtils.applicationContext = applicationContext;
        }
    }

    /**
     * 通过name获取 Bean.
     */
    public static Object getBean(String name) {
        return getApplicationContext().getBean(name);
    }

    /**
     * 通过class获取Bean.
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    /**
     * 通过name,以及Clazz返回指定的Bean
     *
     * @param name
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getBean(String name, Class<T> clazz) {
        return getApplicationContext().getBean(name, clazz);
    }

    /**
     * 获取指定类型的所有bean实例
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> Map<String, T> getBeansOfType(Class<T> clazz) {
        Map<String, T> instances = getApplicationContext().getBeansOfType(clazz);
        return instances;
    }
}
