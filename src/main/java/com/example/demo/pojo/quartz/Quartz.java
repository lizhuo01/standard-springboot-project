package com.example.demo.pojo.quartz;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class Quartz implements Serializable {

    private Integer id;
    private String   jhmc;//计划名称
    private String  jhlx;//计划路线
    private String   jhlb;//计划类别
    private String    xjlx;//巡检类型
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date    dssj;//定时时间
    private String   cron;//定时时间转换的表达式
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date   xjkssj;//巡检开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date   xjjssj;//巡检结束时间
    private String    xjpc;//巡检频次
    private String   xjrwsc;//巡检任务时长
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date jhqysj;//计划启用时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date   jhtysj;//计划停用时间
    private String    zdxjsj;//最短巡检时间
    private String   yqzx;//是否允许延期执行
    private String    zdyqsj;//最大延期时间
    private String   qyzt;//启用状态
    private String    dscl;//定时策略
    private String   creator;
    private String    creator_time;
    private String   updator;
    private String   updator_time;
    private String   delete_flag;

    //计划分类
    private String   jhfl;

    //巡检人
    private List<String> userName;
    //单位
    private String glz;
}
