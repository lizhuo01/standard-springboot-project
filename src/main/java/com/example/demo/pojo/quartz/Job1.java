package com.example.demo.pojo.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 实现Job接口中的execute方法，写定时任务逻辑
 * @author MACHENIKE
 */

public class Job1 implements Job {
    /**
     * 获取任务类
     */

    Dayshift dayshift = SpringApplicationUtils.getBean(Dayshift.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("进入job1=====================");
        dayshift.Tasking();
    }


}