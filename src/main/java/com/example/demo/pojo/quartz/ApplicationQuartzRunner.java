package com.example.demo.pojo.quartz;

import com.example.demo.service.impl.QuartzServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 该类作用为在项目启动时，开启定时任务
 * @author MACHENIKE
 */
@Component
@Slf4j
public class ApplicationQuartzRunner implements ApplicationRunner {

    private Logger logger = LoggerFactory.getLogger(ApplicationQuartzRunner.class);
    @Autowired
    QuartzServiceImpl quartzServiceImpl;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        quartzServiceImpl.contextInitialized();
        logger.debug("定时计划 ——启动了  好耶！");
    }
}