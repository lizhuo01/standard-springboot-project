package com.example.demo.pojo.test;

import java.util.ArrayList;

/**
 * @author T480
 * 多线程测试示例Class
 *
 */
public class concurrency {

    /**
     * 多线程示例：
     * tops：小知识--       1.通过继承Thread类本身。
     *         2.通过实现Runnable接口。
     *         3.通过Callable和Future创建线程池。
     * Thread类本身也是实现了Runnable接口来达到开启线程的目的。我们平时开发，通过实现Runable接口来实现多线程。（理由：接口相比类更加轻量，较少许多隐形的问题；Java属于单继承语言，
     * 尽量把父类这个位置空出来）。
     * ————————————————
     * 版权声明：本文为CSDN博主「A阳光码农」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
     * 原文链接：https://blog.csdn.net/weixin_52014130/article/details/125931366
     */

    public  static  void main(String[] args) {

        //创建MaiPiao类的对象
        MaiPiao m=new MaiPiao();


        ArrayList<String> arrayList = new ArrayList<>();

        for (int i = 0; i <100 ; i++) {
            arrayList.add("第"+i+"张票");
        }

        //创建三个Thread类的对象，把MaiPiao对象作为构造方法的参数，并给出对应的窗口名称
        Thread t1=new Thread(m,"售票口1");
        Thread t2=new Thread(m,"售票口2");
        Thread t3=new Thread(m,"售票口3");
        //启动线程
        t1.start();
        t2.start();
        t3.start();
    }


    public static class MaiPiao implements Runnable {

        {}

        private int tickets =100;
        public synchronized void run() {
            while(true) {
                if(tickets>0) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName()+"正在出售第"+tickets+"张票");
                    tickets--;
                }
            }
        }
    }
}
