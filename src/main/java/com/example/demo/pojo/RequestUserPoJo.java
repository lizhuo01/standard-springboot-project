package com.example.demo.pojo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.common.PojoUtil;
import lombok.Data;

/**
 * @author T480
 */
@Data
public class RequestUserPoJo{

    private Integer id;

    private String name;

    private String age;

    private String email;

    private String gender;

    private Integer phone;

    private String address;

    private String password;
    //第几页
    private Integer page;
    //当前页数量
    private Integer size;

}
