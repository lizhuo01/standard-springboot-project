package com.example.demo.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.demo.common.PojoUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author T480
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user")
public class UserPoJo extends PojoUtil {

    private String id;

    private String name;

    private String age;

    private String email;

    private String gender;

    private String phone;

    private String address;

    private String password;


}
