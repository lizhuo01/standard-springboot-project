package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.pojo.UserPoJo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * @author T480
 */

@Mapper
public interface UserMapper extends BaseMapper<UserPoJo> {
    /***
     *事实上
     * @return
     */
    Integer testString();

    /**
     * 删除功能
     *
     * @param
     * @return
     */
    Integer deleteListUserId(@Param("list") List<String> list);
}
