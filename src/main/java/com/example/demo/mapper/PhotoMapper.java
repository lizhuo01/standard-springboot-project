package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.pojo.PhotoPoJo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.context.annotation.Bean;

/**
 * @author T480
 */
@Mapper
public interface PhotoMapper extends BaseMapper<PhotoPoJo> {


}
