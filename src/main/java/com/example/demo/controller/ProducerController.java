package com.example.demo.controller;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author T480
 * MQ的使用
 */
public class ProducerController {

    public static final String QUEUE_NAME ="hello";

    /**
     * RebbitMQ的使用 : 生产者发送消息
     * @param args
     * @throws IOException
     * @throws TimeoutException
     */
    public static void main(String[] args) throws IOException, TimeoutException {
    //创建一个连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        //工厂ip 连接RabbitMQ的队列
        factory.setHost("127.0.0.1");
        //RabbitMQ账号密码
        factory.setUsername("admin");
        factory.setPassword("admin");
        //创建连接
        Connection connection = factory.newConnection();
        //获取信道进行发消息
        Channel channel = connection.createChannel();
        /**
         * 生成一个队列
         * 1.队列名称
         * 2.队列消息是否持久化(磁盘) 默认情况消息存储在内存中
         * 3.该队列是否只提供一个消费者进行消费 是否进行消息共享,true可以多个消费者消费 false:只能一个消费者消费
         * 4.是否自动删除 最后一个消费者断开连接以后 该队是否自动删除 true自动删除 false 不自动删除
         * 5.其他参数
         */
        String message = "hello world";
        /**
         * 发送一个消费
         * 1.发送到那个交换机
         * 2.路由的key值是哪个 本次设置的key为队列的名称
         * 3.其他参数信息
         * 4.发送消息的消息体 (将消息转为二进制进行发送)
         */
        channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
        System.out.println("生产者消息生产成功--");
    }

}
