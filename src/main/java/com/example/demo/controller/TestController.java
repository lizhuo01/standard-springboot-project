package com.example.demo.controller;


import com.example.demo.common.Response;
import com.example.demo.pojo.UserPoJo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author T480
 */
@RestController
@Api(tags = "测试接口")
@RequestMapping(value = "/test")
@Slf4j
public class TestController {
    private Logger logger = LoggerFactory.getLogger(TestController.class);
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private static ApplicationContext applicationContext;


    @PostMapping(value = "/photo")
    public Response photo(@RequestParam("file")MultipartFile file)   {
        Response response = new Response();


        return response;
    }

    /**
     *日志级别测试接口
     */
    @GetMapping(value = "/test")
    public String  photo() {
        String msg = "日志输出";
        logger.info("slf4j print info msg:{}",msg);
        logger.debug("slf4j print debug msg:{}",msg);
        return msg;
    }

    /**
     *使用Redis做查询
     */
    @GetMapping(value = "/SelectRedis")
    public Object selectRedis() {
        //向redis中存入数据
        ArrayList<Map<String, String>> list = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>(2);
        map.put("name","张三");
        map.put("age","18");
        list.add(map);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("胡图图");
        arrayList.add("翻斗花园");
        //向redis中存入数据,设置时间长度和时间格式,时间到达时,该数据过期
        redisTemplate.opsForValue().set("arrayList",arrayList,10, TimeUnit.SECONDS);
        logger.info("logger info ->>>>>>>>>>>  将数据存入redis中");
         return null;
    }

    /**
     *通过浏览器下载指定的文件
     */
    @RequestMapping("/downloadFile")
    public void url(String filename, HttpServletResponse res) throws Exception {

        filename = "谷粒商城_配置手册.docx";
        String path = "C:\\Users\\T480\\Desktop\\"+filename;
        res.setCharacterEncoding("UTF-8");
        // attachment是以附件的形式下载，inline是浏览器打开
        res.setHeader("Content-Disposition", "attachment;filename="+filename+".pdf");
        res.setContentType("text/plain;UTF-8");
        // 把二进制流放入到响应体中
        ServletOutputStream os = res.getOutputStream();
        File file = new File(path);
        byte[] bytes = FileUtils.readFileToByteArray(file);
        os.write(bytes);
        os.flush();
        os.close();
        }

    /**
     * WORD转为PDF
     * @throws Exception
     */
    @RequestMapping("/pdfConvert")
    public void pdfConvert() throws Exception {
        // docx
        XWPFDocument doc = new XWPFDocument(new FileInputStream("C:\\Users\\T480\\Desktop\\谷粒商城_配置手册.docx"));
        PdfOptions options = PdfOptions.create();
        // pdf
        PdfConverter.getInstance().convert(doc, new FileOutputStream("C:\\Users\\T480\\Desktop\\憨憨.pdf"), options);

    }

    /**
     * file转MultipartFile的方式
     * 例:
     *  File file = new File("static/" + title + ".pdf");
     *                 MultipartFile cMultiFile = getMultipartFile(file);
     */
    public static MultipartFile getMultipartFile(File file) {

        FileItem item = (FileItem) new DiskFileItemFactory().createItem("file"
                , MediaType.MULTIPART_FORM_DATA_VALUE
                , true
                , file.getName());
        try (InputStream input = new FileInputStream(file);
             OutputStream os = item.getOutputStream()) {
            // 流转移
            IOUtils.copy(input, os);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid file: " + e, e);
        }

        return new CommonsMultipartFile(item);
    }

    /**
     * 读取txt文件的内容
     * @param file 想要读取的文件对象
     * @return 返回文件内容
     */
    public static String txt2String(File file){
        StringBuilder result = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                result.append(System.lineSeparator()+s);
            }
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return result.toString();
    }

        //获取指定路径下的文件
        public static void getFile(File[] files,HashMap<String,String> map){
            for(File f : files){
                if(f.isDirectory()){
                    getFile( f.listFiles(), map);
                } else {
                    map.put(f.getAbsolutePath(),f.getName());
                }
            }
        }

    public static void main(String[] args) {

        UserPoJo bean = applicationContext.getBean(UserPoJo.class);

                bean.setId("1");
        System.out.println(bean);
    }

    /**
     * 将上传的图片保存到数据库中
     * @param file 上传的图片
     */
    public static String savePicture(File file){
        StringBuilder result = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                result.append(System.lineSeparator()+s);
            }
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return result.toString();
    }






}
