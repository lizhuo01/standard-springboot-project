package com.example.demo.controller;

import com.example.demo.common.Response;
import com.example.demo.pojo.RequestUserPoJo;
import com.example.demo.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author T480
 */
@RestController
@Slf4j
@Api(tags = "人员新增接口")
@RequestMapping(value = "/user")
public class UserController {

            @Autowired
            private UserService userService;

            /**
             * 根据传入的对象进行内容新增或修改
             * @param requestUserPoJo
             * @return
             */
            @ApiOperation("人员新增和修改")
            @PostMapping(value = "/inserOrUpdate/user")
            public Response inserOrUpdateUser(@RequestBody RequestUserPoJo requestUserPoJo){

                Response response = userService.inserOrUpdateUser(requestUserPoJo);
                return response;
         }


            /**
             * 根据用户Id进行用户删除-逻辑删除
             * @param list
             * @return
             */
            @ApiOperation("根据用户Id进行用户删除-逻辑删除")
            @PostMapping(value = "/deleteByUserId/user")
            public Response deleteByUserId(@RequestBody List<String> list){

                Response response = userService.deleteByUserId(list);
                return response;
            }

            /**
             * 用户列表信息分页查询
             * @param requestUserPoJo
             * @return
             */
            @PostMapping(value = "/selectPageUser/user")
            @ApiOperation("用户列表信息-分页查询")
            public Response selectPageUser(@RequestBody RequestUserPoJo requestUserPoJo){

                Response response = userService.selectPageUser(requestUserPoJo);
                return response;
            }

            /**
             * 用户列表信息分页查询
             * @param requestUserPoJo
             * @return
             */
            @PostMapping(value = "/getName")
            @ApiOperation("用户列表信息-分页查询")
            public Response getName(@RequestBody RequestUserPoJo requestUserPoJo){

                Response response = userService.selectPageUser(requestUserPoJo);
                return response;
            }
}
