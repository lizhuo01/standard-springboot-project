package com.example.demo.controller;

import com.aspose.words.Document;
import com.aspose.words.License;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;


@RestController
@Api(tags = "word转PDF")
@RequestMapping(value = "/wordtopdf")
@Slf4j
public class wordtopdf {


    public static void main(String[] args) {
        wordToPdf("C:\\Users\\T480\\Desktop\\ceshi2.pdf","C:\\Users\\T480\\Desktop\\巡检报告模板.docx");
//        wordToPdf("C:\\Users\\T480\\Desktop\\aaa.doc","C:\\Users\\T480\\Desktop\\ceshi2.pdf");

    }
    public static boolean getLicense() {
        boolean result = false;
        try {
            InputStream is = wordtopdf.class.getClassLoader().getResourceAsStream("license.xml");
            // license.xml应放在..\WebRoot\WEB-INF\classes路径下
            License aposeLic = new License();
            aposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    private static void wordToPdf(String pdfUrl, String docxUrl) {
        if (!wordtopdf.getLicense()) { // 验证License 若不验证则转化出的pdf文档会有水印产生
            return;
        }
        File file = new File(pdfUrl); //新建一个pdf文档
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(file);
            Document doc = new Document(docxUrl); //Address是将要被转化的word文档
            doc.save(os, com.aspose.words.SaveFormat.PDF);

            long now = System.currentTimeMillis();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void url(String filename, HttpServletResponse res) throws Exception {

        filename = "谷粒商城_配置手册.docx";
        String path = "C:\\Users\\T480\\Desktop\\"+filename;
        res.setCharacterEncoding("UTF-8");
        // attachment是以附件的形式下载，inline是浏览器打开
        res.setHeader("Content-Disposition", "attachment;filename="+filename+".pdf");
        res.setContentType("text/plain;UTF-8");
        // 把二进制流放入到响应体中
        ServletOutputStream os = res.getOutputStream();
        File file = new File(path);
        byte[] bytes = FileUtils.readFileToByteArray(file);
        os.write(bytes);
        os.flush();
        os.close();
    }




}